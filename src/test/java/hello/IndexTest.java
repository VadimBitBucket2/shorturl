package hello;

import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.beans.factory.annotation.*;
import org.springframework.test.web.servlet.*;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Random;

import org.slf4j.Logger;
import org.json.JSONObject;
import org.junit.*;
import org.junit.runner.*;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.autoconfigure.web.servlet.*;
import org.springframework.boot.test.context.*;
import org.springframework.http.MediaType;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class IndexTest {
	
	public final static Logger logger = (Logger)LoggerFactory.getLogger(IndexTest.class);

	@Autowired
	public MockMvc mockMvc;
	
	@Test
	public void TestIndex() throws Exception {
        mockMvc.perform(get("/"))
        .andExpect(status().isOk())
        .andDo(print());
	}
	
	@Test
	public void TestCharactersExist() throws Exception {
		mockMvc.perform(get("/characters"))
		.andExpect(status().isOk());
	}
	
	@Test
	public void TestRedirectExists() throws Exception {
		mockMvc.perform(get("/djalkxcmkladkadkxcdaf"))
		.andExpect(status().isOk());
	}
	
	@Test
	public void TestMissionExists() throws Exception {
		mockMvc.perform(get("/mission"))
		.andExpect(status().isOk());
	}
	
	@Test
	public void TestSmileLink() throws Exception {
		
		Random r = new Random();
		
		int value = r.nextInt();
		
		String link = Integer.toString(value);
		
		link += "👾";
		
		JSONObject obj = new JSONObject();
		obj.put("link", "http://www.fileformat.info/info/unicode/char/1f52d/browsertest.htm");
		obj.put("target", link);
		
		logger.error("Hellox +\n" + String.format("\"link\"=?&\"target\"= ":, args));
		
		mockMvc.perform(post("/shrink")
			.contentType(MediaType.APPLICATION_FORM_URLENCODED)
			.content(obj.toString()))
			.andExpect(status().isCreated());
	}
	
}
