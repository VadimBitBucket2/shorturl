package hello;

import static org.mockito.Matchers.charThat;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.assertj.core.internal.cglib.core.CollectionUtils;
import org.hibernate.engine.jdbc.spi.SqlExceptionHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class HelloController {

	public static long linkCounter = 0;
	public static ArrayList<Character> free1Symbols = new ArrayList<Character>();

	public static final Logger logger = LoggerFactory.getLogger(HelloController.class);

	@Autowired
	public JdbcTemplate template;

	@Autowired
	public Access access;

	static String[] linkAlphabet = new String[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d",
			"e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y",
			"z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
			"U", "V", "W", "X", "Y", "Z", "😁", "😂", "😃", "😄", "😅", "😆", "😉", "😊", "😋", "😎", "😍", "😘", "😚",
			"☺", "😐", "😶", "😏", "😣", "😥", "😪", "😫", "😌", "😜", "😝", "😒", "😓", "😔", "😲", "☹", "😖", "😞",
			"😤", "😢", "😭", "😨", "😩", "😰", "😱", "😳", "😵", "😡", "😠", "😇", "😷", "😈", "👿", "👹", "👺", "💀",
			"☠", "👻", "👽", "👾", "💩", "😺", "😸", "😹", "😻", "😼", "😽", "🙀", "😿", "😾", "🙈", "🙉", "🙊", "👦",
			"👧", "👨", "👩", "👴", "👵", "👶", "👼", "⚕", "🎓", "🏫", "⚖", "🌾", "🍳", "🔧", "🏭", "💼", "💻", "🎤",
			"🎨", "✈", "🚀", "🚒", "👮", "♂", "♀", "💂", "👷", "👳", "👱", "🎅", "👸", "👰", "👲", "🙍", "🙎", "🙅",
			"🙆", "💁", "🙋", "🙇", "💆", "💇", "🚶", "🏃", "💃", "👯", "👤", "🏂", "🏄", "🏊", "👫", "💏", "❤", "💋",
			"💑", "👪", "💪", "👈", "👉", "☝", "👆", "👇", "✌", "✋", "👌", "👍", "👎", "✊", "👊", "👋", "👏", "✍", "👐",
			"🙌", "🙏", "💅", "👂", "👃", "👣", "👀", "👅", "👄", "💘", "💓", "💔", "💕", "💖", "💗", "💙", "💚", "💛",
			"💜", "💝", "💞", "💟", "❣", "💌", "💤", "💢", "💣", "💥", "💦", "💨", "💫", "💬", "👓", "👔", "👕", "👖",
			"👗", "👘", "👙", "👚", "👛", "👜", "👝", "🎒", "👞", "👟", "👠", "👡", "👢", "👑", "👒", "🎩", "💄", "💍",
			"💎", "🐵", "🐒", "🐶", "🐩", "🐺", "🐱", "🐯", "🐴", "🐎", "🐮", "🐷", "🐗", "🐽", "🐑", "🐫", "🐘", "🐭",
			"🐹", "🐰", "🐻", "🐨", "🐼", "🐾", "🐔", "🐣", "🐤", "🐥", "🐦", "🐧", "🐸", "🐢", "🐍", "🐲", "🐳", "🐬",
			"🐟", "🐠", "🐡", "🐙", "🐚", "🐌", "🐛", "🐜", "🐝", "🐞", "💐", "🌸", "💮", "🌹", "🌺", "🌻", "🌼", "🌷",
			"🌱", "🌴", "🌵", "🌿", "☘", "🍀", "🍁", "🍂", "🍃", "🍇", "🍈", "🍉", "🍊", "🍌", "🍍", "🍎", "🍏", "🍑",
			"🍒", "🍓", "🍅", "🍆", "🌽", "🍄", "🌰", "🍞", "🍖", "🍗", "🍔", "🍟", "🍕", "🍲", "🍱", "🍘", "🍙", "🍚",
			"🍛", "🍜", "🍝", "🍠", "🍢", "🍣", "🍤", "🍥", "🍡", "🍦", "🍧", "🍨", "🍩", "🍪", "🎂", "🍰", "🍫", "🍬",
			"🍭", "🍮", "🍯", "☕", "🍵", "🍶", "🍷", "🍸", "🍹", "🍺", "🍻", "🍴", "🔪", "🌏", "🗾", "🌋", "🗻", "🏠",
			"🏡", "🏢", "🏣", "🏥", "🏦", "🏨", "🏩", "🏪", "🏬", "🏯", "🏰", "💒", "🗼", "🗽", "⛪", "⛲", "⛺", "🌁",
			"🌃", "🌄", "🌅", "🌆", "🌇", "🌉", "♨", "🌌", "🎠", "🎡", "🎢", "💈", "🎪", "🎭", "🎰", "🚃", "🚄", "🚅",
			"🚇", "🚉", "🚌", "🚑", "🚓", "🚕", "🚗", "🚙", "🚚", "🚲", "🚏", "⛽", "🚨", "🚥", "🚧", "🛑", "⚓", "⛵",
			"🚤", "🚢", "💺", "🚪", "🚽", "🛀", "⌛", "🕛", "🕐", "🕑", "🕒", "🕓", "🕔", "🕕", "🕖", "🕗", "🕘", "🕙",
			"🕚", "🌑", "🌓", "🌔", "🌕", "🌙", "🌛", "☀", "⭐", "🌟", "🌠", "☁", "⛅", "🌀", "🌈", "🌂", "☂", "☔", "⚡",
			"❄", "☃", "⛄", "☄", "🔥", "💧", "🌊", "🎃", "🎄", "🎆", "🎇", "✨", "🎈", "🎉", "🎊", "🎋", "🎍", "🎎", "🎏",
			"🎐", "🎑", "🎀", "🎁", "🎫", "🏆", "⚽", "⚾", "🏀", "🏈", "🎾", "🎱", "🎳", "🎯", "⛳", "🎣", "🎽", "🎿",
			"🎮", "🎲", "♠", "♥", "♦", "♣", "🃏", "🀄", "🎴", "🔊", "📢", "📣", "🔔", "🎼", "🎵", "🎶", "🎧", "📻",
			"🎷", "🎸", "🎹", "🎺", "🎻", "📱", "📲", "☎", "📞", "📟", "📠", "🔋", "🔌", "⌨", "💽", "💾", "💿", "📀",
			"🎥", "🎬", "📺", "📷", "📹", "📼", "🔍", "🔎", "📡", "💡", "🔦", "🏮", "📔", "📕", "📖", "📗", "📘", "📙",
			"📚", "📓", "📒", "📃", "📜", "📄", "📰", "📑", "🔖", "💰", "💴", "💵", "💸", "💳", "💹", "💱", "💲", "✉",
			"📧", "📨", "📩", "📤", "📥", "📦", "📫", "📪", "📮", "✏", "✒", "📝", "📁", "📂", "📅", "📆", "📇", "📈",
			"📉", "📊", "📋", "📌", "📍", "📎", "📏", "📐", "✂", "🔒", "🔓", "🔏", "🔐", "🔑", "🔨", "⚒", "⚔", "🔫",
			"🔩", "⚙", "⚗", "🔗", "💉", "💊", "🚬", "🗿", "🔮", "🏧", "♿", "🚹", "🚺", "🚻", "🚼", "🚾", "⚠", "⛔", "🚫",
			"🚭", "🔞", "☢", "☣", "⬆", "↗", "➡", "↘", "⬇", "↙", "⬅", "↖", "↕", "↔", "↩", "↪", "⤴", "⤵", "🔃", "🔙",
			"🔚", "🔛", "🔜", "🔝", "⚛", "✡", "☸", "☯", "✝", "☦", "☪", "☮", "🔯", "♈", "♉", "♊", "♋", "♌", "♍", "♎",
			"♏", "♐", "♑", "♒", "♓", "⛎", "▶", "◀", "🔼", "🔽", "⏏", "🎦", "📶", "📳", "📴", "♻", "📛", "⚜", "🔰", "🔱",
			"⭕", "✅", "☑", "✔", "✖", "❌", "❎", "➕", "➖", "➗", "➰", "➿", "〽", "✳", "✴", "❇", "‼", "⁉", "❓", "❔", "❕",
			"❗", "〰", "©", "®", "™", "#", "*", "🔟", "💯", "🔠", "🔡", "🔢", "🔣", "🔤", "🅰", "🆎", "🅱", "🆑", "🆒",
			"🆓", "ℹ", "🆔", "Ⓜ", "🆕", "🆖", "🅾", "🆗", "🅿", "🆘", "🆙", "🆚", "🈁", "🈂", "🈷", "🈶", "🈯", "🉐",
			"🈹", "🈚", "🈲", "🉑", "🈸", "🈴", "🈳", "㊗", "㊙", "🈺", "🈵", "▪", "▫", "⬛", "⬜", "🔶", "🔷", "🔸", "🔹",
			"🔺", "🔻", "💠", "🔘", "🔲", "🔳", "⚪", "⚫", "🔴", "🔵", "🏁", "🚩", "🎌", "★", "☆", "☇", "☈", "☉", "☊",
			"☋", "☌", "☍", "☏", "☐", "☒", "☓", "☚", "☛", "☜", "☞", "☟", "☡", "☤", "☥", "☧", "☨", "☩", "☫", "☬", "☭",
			"☻", "☼", "☽", "☾", "☿", "♁", "♃", "♄", "♅", "♆", "♇", "♔", "♕", "♖", "♗", "♘", "♙", "♚", "♛", "♜", "♝",
			"♞", "♟", "♪", "♫", "✁", "✃", "✄", "✆", "✇", "✎", "✐", "✑", "✓", "✕", "✗", "✘", "✙", "✚", "✛", "✜", "✞",
			"✟", "✠", "✢", "✣", "✤", "✥", "✦", "✧", "✩", "✪", "✫", "✬", "✭", "✮", "✯", "✰", "✱", "✲", "✵", "✶", "✷",
			"✸", "✹", "✺", "✻", "✼", "✽", "✾", "✿", "❀", "❁", "❂", "❃", "❅", "❆", "❈", "❉", "❊", "❋", "❍", "❏", "❐",
			"❑", "❒", "❖", "❘", "❙", "❚", "❡", "❥", "❦", "❧", "➔", "➘", "➙", "➚", "➛", "➜", "➝", "➞", "➟", "➠", "➢",
			"➣", "➤", "➥", "➦", "➧", "➨", "➩", "➪", "➫", "➬", "➭", "➮", "➯", "➱", "➲", "➳", "➴", "➵", "➶", "➷", "➸",
			"➹", "➺", "➻", "➼", "➽", "〄", "〷", "〾", "㉿", "฿", "₠", "₡", "₢", "₣", "₤", "₥", "₦", "₧", "₩", "₪", "₫",
			"€", "₭", "₮", "₯", "₰", "₱", "❶", "➀", "➊", "❿", "➉", "➓", "❷", "➁", "➋", "❸", "➂", "➌", "❹", "➃", "➍",
			"❺", "➄", "➎", "❻", "➅", "➏", "❼", "➆", "➐", "❽", "➇", "➑", "❾", "➈", "➒", "〶", "ｰ", "￠", "￡", "￥", "￦",
			"０", "㍘", "１", "¹", "⅟", "½", "⅓", "¼", "⅕", "⅙", "⅛", "㏩", "㋉", "㍢", "㏪", "㋊", "㍣", "㏫", "㋋", "㍤", "㏬",
			"㍥", "㏭", "㍦", "㏮", "㍧", "㏯", "㍨", "㏰", "㍩", "㏱", "㍪", "㏲", "㍫", "㏠", "㋀", "㍙", "２", "²", "⅔", "⅖", "㏳",
			"㍬", "㉑", "㏴", "㍭", "㉒", "㏵", "㍮", "㉓", "㏶", "㍯", "㉔", "㏷", "㍰", "㉕", "㏸", "㉖", "㏹", "㉗", "㏺", "㉘", "㏻",
			"㉙", "㏼", "㏡", "㋁", "㍚", "３", "³", "¾", "⅗", "⅜", "㉚", "㏽", "㉛", "㏾", "㉜", "㉝", "㉞", "㉟", "㊱", "㊲", "㊳",
			"㊴", "㏢", "㋂", "㍛", "４", "⅘", "㊵", "㊶", "㊷", "㊸", "㊹", "㊺", "㊻", "㊼", "㊽", "㊾", "㏣", "㋃", "㍜", "５", "⅚",
			"⅝", "㊿", "㏤", "㋄", "㍝", "６", "㏥", "㋅", "㍞", "７", "⅞", "㏦", "㋆", "㍟", "８", "㏧", "㋇", "㍠", "９", "㏨", "㋈",
			"㍡", "➾", "☰", "☱", "☲", "☳", "☴", "☵", "☶", "☷", "♭", "♯", "〒", "〓", "〠", "¤", "¢", "£", "¥", "‒", "–",
			"—", "―", "〜", "、", "﹅", "﹆", "¡", "¿", "。", "‘", "‚", "‛", "‹", "›", "“", "”", "„", "‟", "〝", "〞", "〟",
			"«", "»", "〈", "〉", "《", "》", "「", "」", "『", "』", "【", "】", "〔", "〕", "〖", "〗", "〘", "〙", "〚", "〛", "﴾",
			"﴿", "§", "¶", "⁄", "‰", "†", "‡", "•", "′", "〃", "※", "°", "←", "→", "↑", "↓", "↨", "⇒", "⇔", "∀", "∂",
			"∃", "∆", "∇", "∈", "∋", "∏", "∐", "∑", "±", "÷", "×", "¬", "¦", "−", "∕", "∙", "√", "∝", "∞", "∟", "∠",
			"∥", "∧", "∨", "∩", "∪", "∫", "∮", "∴", "∵", "∽", "≈", "≒", "≡", "≤", "≥", "≦", "≧", "≪", "≫", "⊂", "⊃",
			"⊆", "⊇", "⊥", "⊿", "⌂", "⌐", "⌒", "⌠", "⌡", "␣", "─", "━", "│", "┃", "┌", "┏", "┐", "┓", "└", "┗", "┘",
			"┛", "├", "┝", "┠", "┣", "┤", "┥", "┨", "┫", "┬", "┯", "┰", "┳", "┴", "┷", "┸", "┻", "┼", "┿", "╂", "╋",
			"═", "║", "╒", "╓", "╔", "╕", "╖", "╗", "╘", "╙", "╚", "╛", "╜", "╝", "╞", "╟", "╠", "╡", "╢", "╣", "╤",
			"╥", "╦", "╧", "╨", "╩", "╪", "╫", "╬", "▀", "▄", "█", "▌", "▐", "░", "▒", "▓", "■", "▬", "▲", "△", "►",
			"▼", "▽", "◄", "◆", "◇", "◊", "○", "◌", "◎", "●", "◘", "◙", "◦", "◯", "⠁", "⠂", "⠃", "⠄", "⠅", "⠆", "⠇",
			"⠈", "⠉", "⠊", "⠋", "⠌", "⠍", "⠎", "⠏", "⠐", "⠑", "⠒", "⠓", "⠔", "⠕", "⠖", "⠗", "⠘", "⠙", "⠚", "⠛", "⠜",
			"⠝", "⠞", "⠟", "⠠", "⠡", "⠢", "⠣", "⠤", "⠥", "⠦", "⠧", "⠨", "⠩", "⠪", "⠫", "⠬", "⠭", "⠮", "⠯", "⠰", "⠱",
			"⠲", "⠳", "⠴", "⠵", "⠶", "⠷", "⠸", "⠹", "⠺", "⠻", "⠼", "⠽", "⠾", "⠿", "⡀", "⡁", "⡂", "⡃", "⡄", "⡅", "⡆",
			"⡇", "⡈", "⡉", "⡊", "⡋", "⡌", "⡍", "⡎", "⡏", "⡐", "⡑", "⡒", "⡓", "⡔", "⡕", "⡖", "⡗", "⡘", "⡙", "⡚", "⡛",
			"⡜", "⡝", "⡞", "⡟", "⡠", "⡡", "⡢", "⡣", "⡤", "⡥", "⡦", "⡧", "⡨", "⡩", "⡪", "⡫", "⡬", "⢀", "⢁", "⢂", "⢃",
			"⢄", "⢅", "⢆", "⢇", "⢈", "ꀀ", "ꀁ", "ꀂ", "ꀃ", "ꈻ", "ꈼ", "ꈽ", "ꈾ", "ꈿ", "ꉀ", "ꉁ", "ꉂ", "ꉃ", "ꉄ", "ꉅ", "ꉒ",
			"ꉓ", "ꉔ", "ꉕ", "ꊯ", "ꊰ", "ꊱ", "ꊲ", "ꊳ", "ꊴ", "ꊵ", "ꋉ", "ꋊ", "ꋋ", "ꋌ", "ꋍ", "ꋎ", "ꋏ", "ꋐ", "ꋑ", "ꋒ", "ꋓ",
			"ꋔ", "ꋕ", "ꋖ", "ꋤ", "ꌕ", "ꌖ", "ꌗ", "ꌘ", "ꌙ", "ꌚ", "ꌛ", "ꌜ", "ꌝ", "ꌞ", "ꑡ", "ꑢ", "ꑣ", "ꑤ", "ꑵ", "ꑶ", "ꒈ",
			"ꒉ", "ꒊ", "꒐", "꒑", "꒒", "꒓", "꒔", "꒕", "꒖", "꒗", "꒘", "꒙", "꒚", "꒛", "꒞", "꒟", "꒠", "꒡", "꒢", "꒣", "꒤",
			"꒥", "꒦", "꒧", "꒮", "꒯", "꒰", "꒱", "꒲", "꒳", "꒴", "꒵", "꒶", "꒷", "꒸", "꒹", "꒺", "꒻", "꒼", "꒽", "꒾", "꒿",
			"꓀", "꓁", "꓂", "꓃", "꓄", "꓅", "꓆", "ɐ", "ɑ", "ɒ", "ʙ", "ƀ", "ɕ", "ʣ", "ʥ", "ʤ", "ɘ", "ɚ", "ɜ", "ɝ", "ɞ",
			"ʚ", "ɤ", "ʩ", "ɡ", "ɢ", "ʛ", "ʜ", "ɦ", "ɧ", "ɪ", "ʝ", "ɟ", "ʄ", "ʞ", "ʪ", "ʫ", "ʟ", "ɫ", "ɬ", "ɭ", "ɮ",
			"ƛ", "ʎ", "ɱ", "ɴ", "ɳ", "ɶ", "ɷ", "ɸ", "ʠ", "ĸ", "ɹ", "ɺ", "ɻ", "ɼ", "ɽ", "ɾ", "ɿ", "ʁ", "ʂ", "ƪ", "ʅ",
			"ʆ", "ʨ", "ƾ", "ʦ", "ʧ", "ƫ", "ʇ", "ʉ", "ɥ", "ɰ", "ʌ", "ʍ", "ʏ", "ƍ", "ʐ", "ʑ", "ƺ", "ʓ", "ƻ", "ʕ", "ʡ",
			"ʢ", "ʖ", "ǀ", "ǁ", "ǂ", "ǃ", "ʗ", "ʘ", "ʬ", "ʭ", };

	public int linkToHours(String link) {
		int len = link.codePointCount(0, link.length());
		int[] hours = new int[] { 1, 24, 24 * 30 };

		if (len <= 3 && len >= 1)
			return hours[len - 1];
		else
			return 8760;
	}

	@RequestMapping(value = "/characters", method = RequestMethod.GET, produces = "text/html;charset=utf-8")
	public String characters() {
		return "characters";
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView index() {

		String res = "";

		// access.Test();

		logger.error("target is xxxx " + res);

		ModelAndView m = new ModelAndView("index", "command", new ShrinkLinkModel());

		return m;
	}
	

	@RequestMapping(value = "/{path}", method = RequestMethod.GET)
	public ModelAndView redirect(@PathVariable String path) {
		Map<String, Object> object;

		try {
			
			object = access.get_source_or_delete(path); 

		} catch (Exception e) {
			throw new LinkExpiredException("Link expired", "https://emojisecret.com/" + path);
		}

		System.out.printf("Source redirect: " + object.get("source"));

		RedirectLinkModel rm = new RedirectLinkModel();

		rm.setLink(object.get("source").toString());

		return new ModelAndView("redirect", "command", rm);
	}

	
	 @RequestMapping("/test") 
	 public String test() { 
		 return "test"; 
	}
	
	 @RequestMapping("/mission") 
	 public String mission() { 
		 return "mission"; 
	}
	 
	 @RequestMapping("/about") 
	 public String about() {
		 return "about"; 
	 }
	 
	 
	 
	 @RequestMapping("/therms-of-service") 
	 public String therms_of_service() {
		 return "therms-of-service"; 
	 }
	 
	 @RequestMapping("/login") 
	 public String login() {
		 return "login"; 
	 }

	@RequestMapping(value = "/shrink", method = RequestMethod.POST)
	public ModelAndView shrink(@ModelAttribute ShrinkLinkModel links) {
		boolean isLocalHost = false;
		InetAddress inetAddress = null;
		boolean resolved80 = false, resolved443 = false;
		ModelAndView modelAndView = null;
		int hoursActive = 0;

		logger.error("Shorten target is " + links.getTarget());
		logger.error("Shorten target chars length " + linkAlphabet.length);
		logger.error("ABC " + links.getLink().codePointCount(0, links.getLink().length()));
		logger.error("ABD " + links.getTarget().codePointCount(0, links.getTarget().length()));
		
		if (links.getTarget().isEmpty()) {
			throw new InvalidSourceLinkException("Invalid link");
		}
		
		if (links.getLink().isEmpty()) {
			throw new InvalidSourceLinkException("Invalid link");
		}
		
		if (links.getTarget().length() > 191) {
			throw new InvalidSourceLinkException("Shortening error 2");
		}
		
		if (links.getTarget().contains("/")) {
			throw new InvalidSourceLinkException("Invalid link");
		}
		

		try {
			URL url = new URL(links.getLink());

			char[] chars = Character.toChars(0x1F600);

			logger.error("Host is " + url.getHost());
			logger.error("Host is " + links.getLink());
			logger.error("Host is " + links.getTarget());
			logger.error("Url encoded is " + new String(chars));

			HttpURLConnection con;

			if (links.getLink().startsWith("http")) {
				con = (HttpURLConnection) url.openConnection();
			} else if (links.getLink().startsWith("https")) {
				con = (HttpsURLConnection) url.openConnection();
			} else {
				throw new InvalidSourceLinkException("Invalid link");
			}

			con.setRequestMethod("GET");
			con.setConnectTimeout(720);
			con.setRequestProperty("User-Agent",
					"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");
			HttpURLConnection.setFollowRedirects(true);

			con.connect();

			int code = con.getResponseCode();

			con.disconnect();

			if (!(code == 200 || code == 302)) {
				throw new InvalidSourceLinkException("Link not reachable 1 code " + code);
			}

			logger.error("code " + code);

			String host = url.getHost();
			host = host.startsWith("www.") ? host.substring(4) : host;

			inetAddress = InetAddress.getByName(host);

			logger.error("Point0");

			if (inetAddress == null)
				throw new InvalidSourceLinkException("Link not reachable 2");

			logger.error("Address is " + inetAddress.toString());

			logger.error("Point1");

			isLocalHost = inetAddress.isLoopbackAddress() || inetAddress.isAnyLocalAddress();

			if (isLocalHost)
				throw new InvalidSourceLinkException("Link not reachable 3 is localhost " + isLocalHost);

			logger.error("Point2");

			logger.error("Target " + links.getTarget());
			
			hoursActive = 5;
			
			System.out.println("Target length " + links.getTarget().codePointCount(0, links.getTarget().length())  );

			boolean exists = false;

			exists = access.check_target_exists(links.getTarget());

			logger.error("Exists equals " + exists);

			if (exists) {
				throw new InvalidSourceLinkException("Target taken x");
			} else {
				try {
					access.insert_link(links.getLink(), links.getTarget(), hoursActive);
				} catch (Exception e) {
					throw new InvalidSourceLinkException(
							e.getMessage() + " Emojing error " + links.getTarget());
				}
			}
		

			{
				ShrinkLinkModel model = new ShrinkLinkModel();
				model.setLink(links.getLink());
				model.setTarget(links.getTarget());

				modelAndView = new ModelAndView("index", "command", model);

				modelAndView.addObject("smilink", "https://emojisecret.com/" + links.getTarget());

				return modelAndView;
			}
		
		} catch (UnknownHostException e) {
			throw new InvalidSourceLinkException(e.getMessage() + " msg 1");
		} catch (IOException e) {
			throw new InvalidSourceLinkException(e.getMessage() + " msg 2");
		} catch (InvalidSourceLinkException e) {
			throw new InvalidSourceLinkException(e.getMessage() + " msg 3");
		}
	}

	private int lengthSurrogate(String target) {

		int length = 0;
		
		int i = 0;
		
		while (i < target.length()) {
			if (Character.isSurrogate(target.charAt(i))) {
				i += 2;
			} else {
				i += 1;
			}
			
			length++;
		}
		
		return length;
	}

	@ExceptionHandler({ InvalidSourceLinkException.class })
	public ModelAndView handleInvalidSourceLinkException(InvalidSourceLinkException ex) {

		ModelAndView model = new ModelAndView("index", "command", new ShrinkLinkModel());

		model.addObject("msg", ex.getMessage());

		return model;
	}

	@ExceptionHandler({ LinkExpiredException.class })
	public ModelAndView linkExpiredHandler(LinkExpiredException e) {
		ModelAndView model = new ModelAndView("expired", "command", e.getLink());

		return model;
	}

}
