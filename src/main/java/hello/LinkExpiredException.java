package hello;

public class LinkExpiredException extends RuntimeException
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LinkExpiredException(String string, String link) {
		super(string);
		
		this.link = link;
	}
	
	private String link;

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}
	
}