package hello;

public class InvalidSourceLinkException extends RuntimeException
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidSourceLinkException(String string) {
		super(string);
	}
	
}