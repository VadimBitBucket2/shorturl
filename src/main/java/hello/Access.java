package hello;

import java.util.Map;

import javax.transaction.Transactional;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
public class Access {
	
	public final static Logger logger = LoggerFactory.getLogger(Access.class);

	@Autowired
	public JdbcTemplate template;
	
	@Transactional
	public void insert_link(String link, String target, int hours) {
		// TODO Auto-generated method stub
		
		logger.error("inserting ordinary");
		template.update(
				"insert into link (`id`, `source`, `target`, `date_added`, `date_expires`, `is_small`, `small_id`) values (0, ?, ?, NOW(), DATE_ADD(NOW(), INTERVAL ? Hour), 0, 0)", new Object[] { link, target, hours });
	}

	@Transactional
	public boolean check_target_exists(String target) {
		// TODO Auto-generated method stub
		boolean exists = false;
		
		try {
			exists = template.queryForObject(
					"select exists "
					+ "(select target from link where target = ?)", 
					new Object[] { target }, boolean.class);
		} catch (Exception e) {
			return false;
		}
		
		return exists;
	}

	public Map<String, Object> get_source_or_delete(String target) {
		
		Map<String, Object> map;
		
		try {
			map = template.queryForMap("select `source`, `date_expires` from link where target = ?", new Object[] {target});
		} catch (Exception e) {
			throw new InvalidSourceLinkException("Target not exists");
		}
		
		return map;
	}
	
}
