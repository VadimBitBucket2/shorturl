<!DOCTYPE HTML>
<html>
<%@ taglib uri='http://www.springframework.org/tags/form' prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.time.OffsetDateTime" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<head> 
    <title>Getting Started: Serving Web Content</title> 
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-16" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href='${pageContext.request.contextPath}/css/index.css' rel='stylesheet' type='text/css' />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet" />
    <script
	  src="https://code.jquery.com/jquery-3.1.1.js"
	  integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA="
	  crossorigin="anonymous">
    </script>
    
</head>
<body class="body-redirect">
	<div class="container">
		<jsp:include page="navigation.jsp" />
	   	<main class="main-redirect">
	   		<div class="redirect-block">
		   		<div class="redirection-title">
		   			 🚀 Mission
		   		</div>
		   		<div class="mission-body">
					<p>Established in 2011 in London, United Kingdon, smilletta was born out of our's unique identity mission of creating innovative products that inspire collaboration.</p> 
		   			<p></p>
		   			<p>After application of extensive research, idea generation tools and countless lessions of brain storming activities, finally a vision of smilletta was born.</p>
		   			<p>With smiletta you can express yourself and your links on a wholy new level of communication.</p>
		   			<p></p>
		   			<p>Link can tell a thousands of stories, and now you a have a tool which can facilitate this story sharing.</p>
	   			</div>
	   		</div>
	   	</main>
	   	<footer class="footer-redirect">
	   		<section class="value-propositions">
	   			<div class="branded-domain">
	   				Branded link
	   			</div>
	   			<div class="pattern-domain">
	   				Patterned link
	   			</div>
	   			<div class="support">
	   				Reach us
	   			</div>
	   		</section>
	   		
	   		<span><%= OffsetDateTime.now().getYear() %></span><span class="copyright">&#x00A9;</span>
	   	</footer>
   	</div>

</body>
</html>
