<%@ page import="java.time.OffsetDateTime"%>
<footer class="footer-redirect">
	   		<section class="value-propositions">
	   			<div class="branded-domain">
	   				Branded link
	   			</div>
	   			<div class="pattern-domain">
	   				Patterned link
	   			</div>
	   			<div class="support">
	   				Reach us
	   			</div>
	   			<div class="support">
	   				Character list
	   			</div>
	   		</section>
	   		
	   		<span><%= OffsetDateTime.now().getYear() %></span><span class="copyright">&#x00A9;</span>
</footer>