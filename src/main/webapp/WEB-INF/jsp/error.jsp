<!DOCTYPE HTML>
<html>
<%@ taglib uri='http://www.springframework.org/tags/form' prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.time.OffsetDateTime" %>
<head> 
    <title>Getting Started: Serving Web Content</title> 
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-16" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="${pageContext.request.contextPath}/css/index.css" rel='stylesheet' type='text/css' />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet" />
    <script
	  src="https://code.jquery.com/jquery-3.1.1.js"
	  integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA="
	  crossorigin="anonymous">
    </script>
    
    
</head>
<body class="body-redirect">
	<div class="container">
		<jsp:include page="navigation.jsp" />
	   	<main class="main-redirect">
	   		<div class="redirect-block">
		   		<div class="redirection-title">
		   			Error
		   		</div>
	   			<span>Invalid path</span>
	   		</div>
	   	</main>
	   	<footer class="footer-redirect">
	   		<section class="value-propositions">
	   			<div class="branded-domain">
	   				Branded link
	   			</div>
	   			<div class="pattern-domain">
	   				Patterned link
	   			</div>
	   			<div class="support">
	   				Reach us
	   			</div>
	   		</section>
	   		
	   		<span><%= OffsetDateTime.now().getYear() %></span><span class="copyright">&#x00A9;</span>
	   	</footer>
   	</div>

</body>
</html>
